Members
Pana Amphaisakul 5488003
Patchara Poonsombat 5488013
Palathip Hongnark 5488039
Nutchanon Phongoen 5488115

Requirement
- Java SDK 1.7.0 or above installed
- jdbm-1.0.jar For btree

Compiling Instruction
1. Open Terminal or Command Prompt
2. Locate the src directory
3. Compile using the following command
	3.1 For terminal(BASH Shell) uses javac -classpath .:../lib/* FTSP.java
	3.2 For Command Promt uses javac -classpath .;../lib/* FTSP.java



Running Instruction
1. Open Terminal or Command Promt
2. Locate the src folder if you want to compile and run
	2.1 Compile the FTSP.java file using the above command
	2.2 run the FTSP for creating the index using the following command java -cp .;../lib/jdbm-1.0.jar FTSP create [indexName] [maildir foler] for example java -cp .;../lib/jdbm-1.0.jar FTSP create enron1 maildir
		*Note that. In the maildir directory, there should be folders of users inside.
	2.3 Run the FTSP for query processing using the command: java -cp .;../lib/jdbm-1.0.jar FTSP create [indexName] [outputName] [query]+ for example java -cp .;../lib/* FTSP search enron1 result I love this project very much
	2.4 The result output file is in the result folder
3.	If you want to run the Program with out compiling, change the directory to the classes folder and do the same procedure in 2.1-2.4

**PS. 1.Note that if you are running on the Terminal BASH shell use : instead of ;.
	  2. The maildir folder must be in the at the same level of src folder. For example.
	  
	  18/03/2014  00:56    <DIR>          .
18/03/2014  00:56    <DIR>          ..
18/03/2014  00:44    <DIR>          classes
18/03/2014  00:32    <DIR>          lib
18/03/2014  00:33    <DIR>          maildir
18/03/2014  00:56             1,403 readme.txt
18/03/2014  00:53    <DIR>          result
18/03/2014  00:41    <DIR>          src