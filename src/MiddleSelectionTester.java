import java.util.ArrayList;

public class MiddleSelectionTester
{
    public static void main(String[] args)
    {
        ArrayList<PostingList> list = new ArrayList<PostingList>();
        int N = 10;
        list.add(new PostingList(1));
        list.add(new PostingList(2));
        list.add(new PostingList(3));
        list.add(new PostingList(4));
        list.add(new PostingList(5));
        list.add(new PostingList(6));
        list.add(new PostingList(7));
        list.add(new PostingList(8));
        list.add(new PostingList(9));
        list.add(new PostingList(10));
        
        System.out.println(displayIDF(list, N));
        
        double m = 60.0;
        list = trimHead(list, m, N);
        list = trimTail(list, m, N);
        System.out.println(displayIDF(list, N));
    }
    
    public static String displayIDF(ArrayList<PostingList> list, int N)
    {
        String s = "";
        for(PostingList p : list)
        {
            s += p.getIDF(N) + "\n";
        }
        return s;
    }
    
    public static ArrayList<PostingList> trimHead(ArrayList<PostingList> list, double middleRanking, int N)
    {
        ArrayList<PostingList> newList = new ArrayList<PostingList>();
        int numberOfRemovedElements = getNumberOfRemovedElements(middleRanking, N)/2;
        for(int i = 0; i<numberOfRemovedElements; i++)
        {
            list.remove(0);
        }
        for(PostingList p : list)
        {
            newList.add(p);
        }
        return newList;
    }
    
    public static ArrayList<PostingList> trimTail(ArrayList<PostingList> list, double middleRanking, int N)
    {
        ArrayList<PostingList> newList = new ArrayList<PostingList>();
        int numberOfRemovedElements = getNumberOfRemovedElements(middleRanking, N)/2;
        for(int i = 0; i<numberOfRemovedElements; i++)
        {
            int lastElement = list.size()-1;
            list.remove(lastElement);
        }
        for(PostingList p : list)
        {
            newList.add(p);
        }
        return newList;
    }
    
    public static int getNumberOfRemovedElements(double middleRanking, int N)
    {
        double inverseOfMiddleRanking = 100.0 - middleRanking;
        double numberOfRemovedElements = (inverseOfMiddleRanking/100.0) * N;
        return (int)Math.floor(numberOfRemovedElements);
    }
}