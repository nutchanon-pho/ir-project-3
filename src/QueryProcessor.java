import java.util.ArrayList;
import jdbm.helper.Tuple;
import jdbm.helper.TupleBrowser;
import java.io.*;
import java.util.Vector;
import java.util.Collections;

public class QueryProcessor
{
    private Index index;
    private int noOfDocuments;
    
    public QueryProcessor(Index index, int noOfDocuments)
    {
        this.index = index;
        this.noOfDocuments = noOfDocuments;
    }
    
    public ArrayList<Result> getResults(ArrayList<String> keywords, double senderZoneWeight, double subjectZoneWeight, double contentZoneWeight) throws IOException
    {
        ArrayList<Result> results = new ArrayList<Result>();
        for(int i=0;i<noOfDocuments;i++) //traverse by documents
        {
            //Initialize Document Vectors
            Vector<Double> senderDocumentVector = new Vector<Double>();
            Vector<Double> subjectDocumentVector = new Vector<Double>();
            Vector<Double> contentDocumentVector = new Vector<Double>();
            //Initialize Query Vectors
            Vector<Double> senderQueryVector = new Vector<Double>();
            Vector<Double> subjectQueryVector = new Vector<Double>();
            Vector<Double> contentQueryVector = new Vector<Double>();
            
            //Document Name
            String docName = "";
            
            for(String currentKeyword : keywords) //traverse by terms
            {
                int currentDocID = i+1;
                PostingList currentTerm = (PostingList)index.find(currentKeyword);
                Posting currentDocument = null;
                
                double senderIDF = 0.0;
                double subjectIDF = 0.0;
                double contentIDF = 0.0;
                if(currentTerm != null)
                {
                    currentDocument = currentTerm.getPostingWithDocID(currentDocID);
                    
                    //IDF
                    senderIDF = currentTerm.getSenderIDF(noOfDocuments);
                    subjectIDF = currentTerm.getSubjectIDF(noOfDocuments);
                    contentIDF = currentTerm.getContentIDF(noOfDocuments);
                }
                
                //TF
                double queryTF = 1;
                double senderTF = 0.0;
                double subjectTF = 0.0;
                double contentTF = 0.0;
                if(currentDocument != null)
                {
                    docName = currentDocument.getDocName();
                    senderTF = currentDocument.getSenderTermFrequency();
                    subjectTF = currentDocument.getSubjectTermFrequency();
                    contentTF = currentDocument.getContentTermFrequency();
                }
                
                //Document Weight
                double senderDocumentWeight = senderTF * senderIDF;
                double subjectDocumentWeight = subjectTF * subjectIDF;
                double contentDocumentWeight = contentTF * contentIDF;
                
                //Query Weight
                double senderQueryWeight = queryTF * senderIDF;
                double subjectQueryWeight = queryTF * subjectIDF;
                double contentQueryWeight = queryTF * contentIDF;
                
                //Add weights to document vectors
                senderDocumentVector.add(senderDocumentWeight);
                subjectDocumentVector.add(subjectDocumentWeight);
                contentDocumentVector.add(contentDocumentWeight);
                
                //Add weights to query vectors
                senderQueryVector.add(senderQueryWeight);
                subjectQueryVector.add(subjectQueryWeight);
                contentQueryVector.add(contentQueryWeight);
                
            }
            //Normalized query vectors
            senderQueryVector = cosineNormalize(senderQueryVector);
            subjectQueryVector = cosineNormalize(subjectQueryVector);
            contentQueryVector = cosineNormalize(contentQueryVector);
            //Compute score
            double senderZoneScore = computeScore(senderDocumentVector, senderQueryVector) * senderZoneWeight;
            double subjectZoneScore = computeScore(subjectDocumentVector, subjectQueryVector) * subjectZoneWeight;
            double contentZoneScore = computeScore(contentDocumentVector, contentQueryVector) * contentZoneWeight;
            double finalScore = roundToFourDecimalPoints(senderZoneScore + subjectZoneScore + contentZoneScore);
            if(finalScore != 0.0)
            {
                Result result = new Result(docName, finalScore);
                results.add(result);
            }
        }
        Collections.sort(results, new ResultComparator());
        return results;
    }
    
    public static double computeScore(Vector<Double> v1, Vector<Double> v2)
    {
        int noOfTerms = v1.size(); //Loop checking term by term
        double score = 0;
        double normV1 = 0;
        double normV2 = 0;
        for(int i=0 ;i<noOfTerms; i++)
        {
            score += v1.get(i)*v2.get(i);
            
            normV1 += Math.pow(v1.get(i),2);
            normV2 += Math.pow(v2.get(i),2);
        }
        return score;
    }
    
    public static Vector<Double> cosineNormalize(Vector<Double> vector)
    {
        double totalWeight = 0.0;
        for(double weight : vector)
        {
            totalWeight += Math.pow(weight,2);
        }
        double sqrtTotalWeight = Math.sqrt(totalWeight);
        Vector<Double> normalizedVector = new Vector<Double>();
        for(int i=0;i<vector.size();i++)
        {
            double newWeight = 0.0;
            if(sqrtTotalWeight != 0)
                newWeight = vector.get(i)/sqrtTotalWeight;
            normalizedVector.add(newWeight);
        }
        return normalizedVector;
    }
    
    private double roundToFourDecimalPoints(double d)
    {
        return Math.floor(d * 10000) / 10000.0;
    }
}