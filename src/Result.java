public class Result
{
    private String docName;
    private double score;
    
    public Result(String docName, double score)
    {
        this.docName = docName;
        this.score = score;
    }
    
    public String getDocName()
    {
        return docName;
    }
    
    public double getScore()
    {
        return score;
    }
    
    public String toString()
    {
        String s = "Document Name : " + docName + " Score : " + score;
        return s;
    }
}