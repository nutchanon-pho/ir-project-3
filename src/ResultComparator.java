import java.util.Comparator;

public class ResultComparator implements Comparator<Result>
{
    @Override
    public int compare(Result r1, Result r2) {
        return (int)((r2.getScore() - r1.getScore())*10000);
    }
}